#!/bin/bash
# Meant to be called by install_android.sh
# Added GPT feature for Android 7.1.2_r9

LOG_STDOUT="/opt/install_stdout.log"
LOG_STDERR="/opt/install_stderr.log"
#LOG_STDOUT="/dev/kmesg"
#LOG_STDERR="/dev/kmesg"


DEPLOY_LINK=/opt/images/Linux
#ISO_DEST_LINK=$2
DEST_DEV=/dev/$1

SPL_NAME_TGT=SPL-nand
UBOOT_NAME_TGT=u-boot.img-nand

DTB_NAME=imx6q-var-som-res.dtb
DTB_NAME_SOLO=imx6dl-var-som-res.dtb
UIMAGE_NAME=uImage
ROOTFS_NAME=core-image-base-var-som-mx6.ext4
ROOTFS_SIZE=( $(du -sLb /$DEPLOY_LINK/$ROOTFS_NAME | grep -oE "\b[0-9]+") )

#Пути параметры временных файлов

#Параметры второго раздела
ROOTFS_ST_OFSET=`expr 24578 \* 512`
ROOTFS_FREE_SPACE=`expr 100 \* 1024 \* 1024`
INSTALL_STEP=0

echo "================================================" >> $LOG_STDERR
echo "============= ЛОГ НАЧИНАЕТСЯ ЗДЕСЬ =============" >> $LOG_STDERR
echo "================================================" >> $LOG_STDERR
echo "================================================" >> $LOG_STDOUT
echo "============= ЛОГ НАЧИНАЕТСЯ ЗДЕСЬ =============" >> $LOG_STDOUT
echo "================================================" >> $LOG_STDOUT

function die
{
	echo "ОШИБКА: Что-то пошло не так! Посмотрите в файлы $LOG_STDOUT и $LOG_STDERR для подробной информации"
}

function die_warn
{
	echo "ПРЕДУПРЕЖДЕНИЕ: Что-то пошло не так! Посмотрите в файлы $LOG_STDOUT и $LOG_STDERR для подробной информации"
}

function delete_device
{

	echo " "
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Удаление текущих разделов"
	
	dd if=/dev/zero of=${DEST_DEV}p0 bs=1024 count=1024 >> $LOG_STDOUT 2>> $LOG_STDERR || true
	dd if=/dev/zero of=${DEST_DEV}p1 bs=1024 count=1024 >> $LOG_STDOUT 2>> $LOG_STDERR || true
	dd if=/dev/zero of=${DEST_DEV}   bs=1M count=4 >> $LOG_STDOUT 2>> $LOG_STDERR || true
	sync

	sync; sleep 1
}

function create_parts
{

	echo " "
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Содание новых разделов"
	
	TMPFILESIZE=`expr ${ROOTFS_ST_OFSET} + ${ROOTFS_SIZE} + ${ROOTFS_FREE_SPACE} `
	
	USERFS_ST_OFSET=`expr ${TMPFILESIZE} + 1048576`
	
	umount ${DEST_DEV}p*
        sfdisk --force -uS ${DEST_DEV} << EOF
2186, 2048, 83
4234, 20480, 83
24714, 20480, 83
45194, ,5;
45195, 2048000 ,83
2093196, 2048000, 83
4141197, 20480, 83
4161678, , 83;
EOF
	kpartx -u ${DEST_DEV}
}

function install_bootloader
{	
	echo " "
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Установка загрузчика"
	
	
	echo " ... : Очистка NAND"
	flash_erase /dev/mtd0 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	flash_erase /dev/mtd1 0 0 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	
	echo " ... : Установка NAND загрузчика"
	
	kobs-ng init -x ${DEPLOY_LINK}/${SPL_NAME_TGT} --search_exponent=1 -v >> $LOG_STDOUT 2>> $LOG_STDERR || die
	nandwrite -p /dev/mtd1 ${DEPLOY_LINK}/${UBOOT_NAME_TGT} >> $LOG_STDOUT 2>> $LOG_STDERR || die
}

function install_linux
{
	echo " "
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Установка загрузчика и ядра Linux"
	
	
	echo " ... : Установка раздела ядра: $bootimage_file"
	umount ${DEST_DEV}p*
	mkdir /mnt/BOOT-VARMX6
	
	mkfs.ext4 ${DEST_DEV}p2 -FFL kernel1
	mount ${DEST_DEV}p2 /mnt/BOOT-VARMX6 -t ext4
	
	cp ${DEPLOY_LINK}/${DTB_NAME} /mnt/BOOT-VARMX6/$DTB_NAME
	cp ${DEPLOY_LINK}/${DTB_NAME_SOLO} /mnt/BOOT-VARMX6/${DTB_NAME_SOLO}
	cp ${DEPLOY_LINK}/${UIMAGE_NAME} /mnt/BOOT-VARMX6/$UIMAGE_NAME
	umount -dl ${DEST_DEV}p2
	sync
	
	rm -Rf /mnt/BOOT-VARMX6
	sync
	
	echo " ... : Установка образа rootfs: ${DEPLOY_LINK}/${ROOTFS_NAME}"
	dd if=${DEPLOY_LINK}/${ROOTFS_NAME} of=${DEST_DEV}p5 >> $LOG_STDOUT 2>> $LOG_STDERR || die
	sync
	
	
	#e2label /dev/${DEST_DEV}p3 kernel2
	e2label ${DEST_DEV}p5 root1
	#e2label /dev/${DEST_DEV}p6 root2
	
	echo " ... : Установка точек монтирования раздела root1"
	mkdir /mnt/root1
	mount ${DEST_DEV}p5 /mnt/root1 -t ext4
	 mkdir /mnt/root1/data
	 mkdir /mnt/root1/boot/cfg
	 mkdir /mnt/tmp
	 cp -r /mnt/root1/etc/tracker-default-cfg/* /mnt/tmp
	umount -dl ${DEST_DEV}p5
	sync	
	rm -Rf /mnt/root1
	sync
	
	sleep 1
}

function setup_other_fs
{
	echo " "
	INSTALL_STEP=`expr ${INSTALL_STEP} + 1`
	echo " ШАГ ${INSTALL_STEP}: Проверка/Раворачивание пользовательских фаловых сиситем."
	
	echo " ... : Проверка файловой системы CFG: на ${DEST_DEV}p1"
	fsck.ext2 -n ${DEST_DEV}p1
	if [ $? -eq 0 ]
	then
	 echo " ... : CFG: на ${DEST_DEV}p1 сушествует и исправна оставляем без изменений"
	else
	 echo " ... : CFG: на ${DEST_DEV}p1 ИМЕЕТ СБОЙ с кодом :${?} ПЕРЕФОРМАТИРУЕМ!"
	 sync
	 sleep 1
	 mkfs.ext2 -FFL CFG ${DEST_DEV}p1
	 sync
	fi

	sleep 1
	echo " ... : Проверка файловой системы userid: на ${DEST_DEV}p7"
	fsck.ext4 -n ${DEST_DEV}p7
	if [ $? -eq 0 ]
	then
	 echo " ... : userid на ${DEST_DEV}p7 сушествует и исправна оставляем без изменений"
	else
	 echo " ... : userid на ${DEST_DEV}p7 ИМЕЕТ СБОЙ с кодом :${?} ПЕРЕФОРМАТИРУЕМ!"
	 sync
	 sleep 1
	 mkfs.ext4 -FFL userid ${DEST_DEV}p7
	 sync
	fi
	
	sleep 1
	echo " ... : Установка стандартных файлов конфигураций на userid"
	mkdir /mnt/userid
	mount ${DEST_DEV}p7 /mnt/userid -t ext4	
	 cp -r /mnt/tmp/* /mnt/userid
	 rm -Rf /mnt/tmp

#	 echo " ... : Копируем ключи для доступа к ssh-tunnel на userid"
#	 mkdir -p /mnt/userid/ssh-tunnel/.ssh
#	 cp -r /opt/images/ssh-tunnel/.ssh/* /mnt/userid/ssh-tunnel/.ssh

	 sync
	umount -dl ${DEST_DEV}p7
	sync
	rm -Rf /mnt/userid
	
	sleep 1
	echo " ... : Создание файловой системы userdt: на ${DEST_DEV}p8"
	mkfs.ext4 -FFL userdt ${DEST_DEV}p8
	sync
	
	sleep 1
	echo " ... : Установка точек монтирования раздела userdt"
	mkdir /mnt/userdt
	mount ${DEST_DEV}p8 /mnt/userdt -t ext4	
	 mkdir /mnt/userdt/cfg
	 mkdir /mnt/userdt/boot_cfg
	 sync
	umount -dl ${DEST_DEV}p8
	sync	
	rm -Rf /mnt/userdt

}
#check_images


install_bootloader
delete_device
create_parts
install_linux
setup_other_fs

