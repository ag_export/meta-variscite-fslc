#!/bin/bash

#################################################################
#                                                               #
# Скрипт позволяет автоматизировать часто повторяющиеся задачи. #
# Все сценарии в данном файле рассчитаны на сборку Android для  #
# устройства МК монитора.                                       #
#                                                               #
#################################################################

BUILD_DISTRO=core-image-base

items=() # Массив ключей и текстов для пунктов меню.
nodes=() # Массив блочных устройств и их размеров.

# Обновляет массив nodes.
nodes_array_update() {
	nodes=( $(lsblk | grep -E 'sd[a-z] ' | awk '{ print $1, "[" $4 "]" }') )
}

# Возвращает последнее блочное устройство, доступное в системе.
get_last_node() {
	nodes_array_update
	echo ${nodes[${#nodes[@]}-2]}
}

# item_add <тег> <текст_пункта_меню>
# Добавляет новый элемент в в меню.
item_add() {
	local menu_index=$1
	local title=$2

	local index=$(( $menu_index * 2 ))
	items[$index]=$1
	items[$index+1]="$title"
}

# item_title_by_index <тег_пункта_меню>
# Возвращает текст пункта меню по его тегу.
item_title_by_index() {
	echo ${items[$1*2+1]}
}

# Отображает меню доступных сценариев.
# Возвращает тег выбранного пользователем  пункта меню.
show_menu() {
	# Количество пунктов меню в массиве.
	# В массиве записаны последовательно индекс элемент - текст - индекс...,
	#	поэтому размер массива делим пополам.
	local count=$(( ${#items[@]} / 2 ))

	# Список элементов массива.
	local items_list="${items[@]}"

	local cols=79
	local rows=$(( $count + 7 ))
	
	dialog --title "$title" --default-item $LAST_ITEM --menu "Выберите один из пунктов:" $rows $cols $count "${items[@]}" 2>&1 >/dev/tty
}

# Отображает меню выбора блочных устройств.
# Возвращает выбранное пользователем блочное устройство.
select_block_device() {
	local count=$(( ${#nodes[@]} / 2 ))
	local items_list="${nodes[@]}"
	local title="Выберите блочное устройство для записи на SD-карту"
	local cols=79
	local rows=$(( $count + 7 ))

	nodes_array_update
	dialog --title "$title" --default-item $LAST_NODE --menu "Выберите один из пунктов:" $rows $cols $count "${nodes[@]}" 2>&1 >/dev/tty
}

# Инициализирует переменные для сборки
script_setup_build_env() {
	local RETURNDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

	export MACHINE=var-som-mx6
	export DISTRO=fslc-framebuffer

	source setup-environment build_framebufer
}

# По идее пишит спикером, но на деле выводит уведомление от терминала
beep() {
	echo -e '\a'
}

script_build() {
	#bitbake -c cleanall core-image-minimal не уверен что работет как надо
	#bitbake -c cleansstate virtual/bootloader virtual/kernel эта штука работат
	#bitbake core-image-minimal
	bitbake "$BUILD_DISTRO"
	#bitbake core-image-minimal -c deploy тестить надо
	#bitbake core-image-minimal -c compile -f тестить надо
	#bitbake core-image-minimal -c imx_v7_var_defconfig -f Собрать конкретный конфиг ядра
	cd ../
}

menu_item_1_selected() {
	# script_setup_build_env
	script_build
}

menu_item_2_selected() {
	bitbake -c cleanall u-boot-variscite linux-variscite
}

menu_item_3_selected() {
	cd ../
	mkdir -pv ${PWD}/out_iso
	${PWD}/sources/meta-variscite-fslc/yocto_iso_creator.sh ${PWD}/build_framebufer/tmp/deploy/images/var-som-mx6 ${PWD}/out_iso
}

menu_item_4_selected() {
	repo sync -j5
}

#/home/modelkin/Foond_Git/SDU/var-fslc-yocto/build_framebufer/tmp/deploy/images/var-som-mx6/

menu_item_q_selected() {
	:
}

# Точка входа в сценарий.
main() {
	# Если переменные окружения не были настроены, то настраиваем.
	if [ -z "${DISTRO}" ]; then script_setup_build_env; fi

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM="0"; fi
	if [ -z "${LAST_NODE}" ]; then LAST_NODE="$(get_last_node)"; fi

	item_add 1 "Сборка дистрибутива Yocto"
	item_add 2 "Очистка сборки Yocto(нужно при изменениях ядра/загрузчика)"
	item_add 3 "Сборка образа для запуска образа с SD"
	item_add 4 "Синхронизация дерева исходного кода(repo)"
	item_add q "Выйти"
	LAST_ITEM=$(show_menu)

	if [ -z "${LAST_ITEM}" ]; then LAST_ITEM='q'; fi
	clear
	menu_item_${LAST_ITEM}_selected
	beep # Показать уведомление, что мы завершили работу
}

main
