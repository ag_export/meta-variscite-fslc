SUMMARY = "Intel EEPROM Access Tool"
AUTHOR = "Intel"
DESCRIPTION = "Intel EEPROM Access Tool"
HOMEPAGE = "https://intel.com"

LICENSE = "CLOSED"
RDEPENDS_${PN} = ""
DEPENDS += "${RDEPENDS_${PN}}"
SRC_URI = "file://./"

S = "${WORKDIR}"

do_compile() {
	make
}

do_install() {
	install -d ${D}/usr/bin
	install -m 0755 ${WORKDIR}/EepromAccessTool ${D}/usr/bin
	
	mkdir -p ${D}/usr/share/EepromAccessTool/
	cp -r flash_images ${D}/usr/share/EepromAccessTool/
}

FILES_${PN} = "/usr/bin/EepromAccessTool /usr/share/EepromAccessTool/"

